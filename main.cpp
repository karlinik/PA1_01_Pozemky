#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include<math.h>

using namespace std;

double perimetr(double *rozmery, int pocetDvojic);
double area(double *rozmery, int pocetDvojic);
void porovnej(double obvod1, double obvod2, double obsah1, double obsah2);
int main()
{
    int pocetDvojic1=0, pocetDvojic2=0;
    int i;
    double obvod1, obvod2, obsah1, obsah2;
    double rozmery1[100];
    double rozmery2[100];

printf("Pozemek #1:\n");
//Nacitani, dokud neni na vstupu dvojice cisel, ktera se rovna prvni dvojici
for (i=0;i<50;i+=2)
{
    if (scanf("%lf %lf", &rozmery1[i], &rozmery1[i+1])!=2)
    {
        printf("Nespravny vstup.\n");
        return 1;
    }
    pocetDvojic1++;
    if (i>=2)
    {
        if (rozmery1[i]==rozmery1[0] && rozmery1[i+1]==rozmery1[1])
        break;
    }
}
//Kontrola nacteni dvojic vypisem
/*printf("Pocet dvojic1: %d\n", pocetDvojic1);
for (i=0;i<2*pocetDvojic1;i++)
{
    printf("Rozmer%d: %f\n",i, rozmery1[i]);
}*/
obvod1 =perimetr(rozmery1, pocetDvojic1);
obsah1 =area(rozmery1,pocetDvojic1);

/*printf("obvod1: %.30f\n", obvod1);
printf("obsah1: %.30f\n", obsah1);
printf("******************************\n");*/

/*-------------------------------------------------------------*/
printf("Pozemek #2:\n");
//Nacitani, dokud neni na vstupu dvojice cisel, ktera se rovna prvni dvojici
for (i=0;i<50;i+=2)
{
    if (scanf("%lf %lf", &rozmery2[i], &rozmery2[i+1])!=2)
    {
        printf("Nespravny vstup.\n");
        return 1;
    }
    pocetDvojic2++;
    if (i>=2)
    {
        if (rozmery2[i]==rozmery2[0] && rozmery2[i+1]==rozmery2[1])
        break;
    }
}
obvod2 =perimetr(rozmery2, pocetDvojic2);
obsah2 =area(rozmery2,pocetDvojic2);

porovnej(obvod1,obvod2,obsah1,obsah2);
    return 0;
}

double perimetr(double *rozmery, int pocetDvojic)
{
    double perimetr=0;
    int i;
    for (i=0;i<2*pocetDvojic-2;i+=2)
    {
    //printf("((%f-%f)*(%f-%f))+((%f-%f)*(%f-%f))\n", rozmery[i+2], rozmery[i], rozmery[i+2], rozmery[i], rozmery[i+3], rozmery[i+1], rozmery[i+3], rozmery[i+1]);
    perimetr+=sqrt(((rozmery[i+2]-rozmery[i])*(rozmery[i+2]-rozmery[i]))+((rozmery[i+3]-rozmery[i+1])*(rozmery[i+3]-rozmery[i+1])));
    }
    return perimetr;
}
double area(double *rozmery, int pocetDvojic)
{

    double soucet=0;

    int i;
    for (i=0;i<2*pocetDvojic-2;i+=2)
    {
    soucet+=((rozmery[i]*rozmery[i+3])-(rozmery[i+1]*rozmery[i+2]));
    }
    return fabs(soucet/2);
}

void porovnej(double obvod1, double obvod2, double obsah1, double obsah2)
{

    /*printf("Obvod1: %g\n obvod2: %g\n", obvod1,obvod2);
    printf("Obsah1: %g\nObsah2: %g", obsah1, obsah2);
    printf("Rozdil obvodu: %g\n", fabs(obvod1-obvod2));
    printf("Rozdil obsahu: %g\n", fabs(obsah1-obsah2));*/

if (obsah1==obsah2 || fabs(obsah1-obsah2)<1e-10)
{
    printf("Pozemky maji stejnou vymeru: %g\n", obsah1);
}else if (obsah1>obsah2)
{
    printf("Rozloha pozemku #1 (= %g) je vetsi nez rozloha pozemku #2 (= %g).\n", obsah1, obsah2);
}else printf("Rozloha pozemku #2 (= %g) je vetsi nez rozloha pozemku #1 (= %g).\n", obsah2, obsah1);

//printf("Obvod1: %f\n obvod2: %f\n",obvod1,obvod2);
//printf("Rozdil: %g\n",fabs(obvod1/1e70-obvod2/1e70));

if (obvod1==obvod2 || fabs(obvod1-obvod2)<1e-10)
{
    printf("Hranice maji stejnou delku: %g\n", obvod1);
}else if (obvod1>obvod2)
{
    printf("Hranice #1 (= %g) je delsi nez hranice #2 (= %g).\n", obvod1,obvod2);
}else printf("Hranice #2 (= %g) je delsi nez hranice #1 (= %g).\n", obvod2, obvod1);
}
